const guardarEstudiante = async (req, res) => {
    const {cedula, nombres, apellidos, semestre, paralelo} = req.body

    const expresiones = {
        nombres: /^([a-zA-Z]{4,})\s([a-zA-Z]+)*$/, // Letras y espacios, pueden llevar acentos.
        cedula: /^\d{10}$/,
        semestre: /^[A-Za-z]{2,40}/,
        paralelo: /^[A-Za-z]{2,10}/
    }

    if(!expresiones.nombres.test(nombre)){
        res.json({'error': 'Ingrese los nombres completos'})
    }else  if(!expresiones.nombres.test(apellido)){
        res.json({'error': 'Ingrese los apellidos completos'})
    }else if(!expresiones.cedula.test(cedula)){
        res.json({'error': 'La cedula debe tener 10 digitos'})
    }else if(!expresiones.semestre.test(semestre)){
        res.json({'error': 'El semestre debe tener minimo 2 caracteres y maximo 40'})
    }else if(!expresiones.paralelo.test(paralelo)){
        res.json({'error': 'El paralelo debe tener minimo 2 caracteres y maximo 10'})
    }else{
        try {
            await pool.query(
                "INSERT INTO estudiante(cedula, nombres, apellidos,  paralelo, semestre) VALUES($1, $2, $3, $4, $5) RETURNING *",
                [cedula, nombres, apellidos, paralelo, semestre],
            (error, result) =>{
                    if(error){
                        res.json({'error': error})
                    }else{
                        res.json(result.rows)
                    }
                }
            );
        } catch (error) {
            res.json({'error': error})
        }
    }
    
}